package ru.ulstu.is.sbapp.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDepartmentDto;
import ru.ulstu.is.sbapp.company.service.DepartmentService;

@Component
public class DepartmentByIdConverter implements Converter<String, EmployeeDepartmentDto> {

    private DepartmentService departmentService;

    @Autowired
    public DepartmentByIdConverter(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    public EmployeeDepartmentDto convert(String id) {
        return new EmployeeDepartmentDto(departmentService.findDepartment(Long.parseLong(id)));
    }
}
