package ru.ulstu.is.sbapp.company.controller.dto;

import ru.ulstu.is.sbapp.company.model.Employee;

public class DepartmentEmployeeDto {
    private Long id;
    private String name;
    public DepartmentEmployeeDto() {

    }

    public DepartmentEmployeeDto(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getFirstName()+" " +employee.getLastName();
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString() {
        return getName();
    }
}
