package ru.ulstu.is.sbapp.company.controller.mvc;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.controller.dto.PositionDto;
import ru.ulstu.is.sbapp.company.service.PositionService;

import javax.validation.Valid;

@Controller
@RequestMapping("/position")
@Secured({ "ROLE_ADMIN", "ROLE_POSITION_MANAGER" })
public class PositionMvcController {
    private final PositionService positionService;
    public PositionMvcController(PositionService positionService){
        this.positionService=positionService;
    }
    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public String getPositions(Model model) {
        model.addAttribute("positions",
                positionService.findAllPositions().stream()
                        .map(PositionDto::new)
                        .toList());
        return "position";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editPosition(@PathVariable(required = false) Long id,
                               Model model) {
        if (id == null || id <= 0) {
            model.addAttribute("positionDto", new PositionDto());
        } else {
            model.addAttribute("positionId", id);
            model.addAttribute("positionDto", new PositionDto(positionService.findPosition(id)));
        }
        return "position-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String savePosition(@PathVariable(required = false) Long id,
                               @ModelAttribute @Valid PositionDto positionDto,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "position-edit";
        }
        if (id == null || id <= 0) {
            positionService.addPosition(positionDto.getName(),positionDto.getSalary());
        } else {
            positionService.updatePosition(positionDto);
        }
        return "redirect:/position";
    }

    @PostMapping("/delete/{id}")
    public String deletePosition(@PathVariable Long id) {
        positionService.deletePosition(id);
        return "redirect:/position";
    }
}
