package ru.ulstu.is.sbapp.company.controller.rest;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.configuration.WebConfiguration;
import ru.ulstu.is.sbapp.company.controller.dto.PositionDto;
import ru.ulstu.is.sbapp.company.service.PositionService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API +"/position")
public class PositionController {

    private final PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @GetMapping("/{id}")
    public PositionDto getPosition(@PathVariable Long id) {
        return new PositionDto(positionService.findPosition(id));
    }

    @GetMapping("/")
    public List<PositionDto> getPositions() {
        return positionService.findAllPositions().stream().map(PositionDto::new).toList();
    }

    @PostMapping("/")
    public PositionDto createPosition(@RequestParam("name") String name,
                                   @RequestParam("salary") int salary) {
        return new PositionDto(positionService.addPosition(name, salary));
    }

    @PutMapping("/{id}")
    public PositionDto updatePosition(@RequestBody @Valid PositionDto positionDto) {
        return positionService.updatePosition(positionDto);
    }

    @DeleteMapping("/{id}")
    public PositionDto deletePosition(@PathVariable Long id) {
        return new PositionDto(positionService.deletePosition(id));
    }

}
