package ru.ulstu.is.sbapp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.is.sbapp.company.model.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {
}
