package ru.ulstu.is.sbapp.user.model;

import org.springframework.security.core.GrantedAuthority;

public enum UserRole implements GrantedAuthority {
    ADMIN("Администратор"),
    USER("Пользователь"),
    POSITION_MANAGER("Управляющий должностями"),
    EMPLOYEE_MANAGER("Управляющий работниками"),
    DEPARTMENT_MANAGER("Управляющий отделами");

    private static final String PREFIX = "ROLE_";

    @Override
    public String getAuthority() {
        return PREFIX + this.name();
    }
    private final String displayValue;

    UserRole(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public static final class AsString {
        public static final String ADMIN = PREFIX + "ADMIN";
        public static final String USER = PREFIX + "USER";
        public static final String POSITION_MANAGER = PREFIX + "POSITION_MANAGER";
        public static final String EMPLOYEE_MANAGER = PREFIX + "EMPLOYEE_MANAGER";
        public static final String DEPARTMENT_MANAGER = PREFIX + "DEPARTMENT_MANAGER";
    }
}
